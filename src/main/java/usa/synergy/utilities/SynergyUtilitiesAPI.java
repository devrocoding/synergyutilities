package usa.synergy.utilities;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;
import usa.synergy.utilities.assets.command.CommandManager;
import usa.synergy.utilities.assets.gui.GuiManager;
import usa.synergy.utilities.assets.runnable.RunnableManager;
import usa.synergy.utilities.assets.version.VersionManager;

public class SynergyUtilitiesAPI {

    @Getter
    private JavaPlugin javaPlugin;

    @Getter
    private GuiManager guiManager;
    @Getter
    private CommandManager commandManager;
    @Getter
    private VersionManager versionManager;
    @Getter
    private RunnableManager runnableManager;

    @Getter
    public static SynergyUtilitiesAPI api;

    public SynergyUtilitiesAPI(JavaPlugin javaPlugin){
        api = this;
        this.javaPlugin = javaPlugin;

        this.guiManager = new GuiManager(javaPlugin);
        this.commandManager = new CommandManager(javaPlugin);
        this.versionManager = new VersionManager(javaPlugin);
        this.runnableManager = new RunnableManager(javaPlugin);
    }

}
