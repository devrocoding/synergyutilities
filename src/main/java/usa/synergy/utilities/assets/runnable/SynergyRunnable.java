package usa.synergy.utilities.assets.runnable;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.function.Consumer;

public class SynergyRunnable extends BukkitRunnable implements Cloneable {

    private final JavaPlugin plugin;
    private final RunnableManager runnableManager;
    @Getter
    private final String name;
    @Getter
    private final Consumer<JavaPlugin> run;

    public SynergyRunnable(JavaPlugin plugin, RunnableManager runnableManager, String name, Consumer<JavaPlugin> run) {
        this.plugin = plugin;
        this.runnableManager = runnableManager;
        this.name = name;
        this.run = run;
    }

    @Override
    public void run() {
        run.accept(plugin);
    }

    @Override
    public synchronized void cancel() throws IllegalStateException {
        super.cancel();
        runnableManager.getRunnables().remove(name);
    }

    public SynergyRunnable clone() {
        try {
            return (SynergyRunnable) super.clone();
        } catch (Exception ex) {
            return null;
        }
    }
}
