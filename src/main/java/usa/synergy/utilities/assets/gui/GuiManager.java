package usa.synergy.utilities.assets.gui;

import com.google.common.collect.Lists;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.java.JavaPlugin;
import usa.synergy.utilities.Module;

import java.util.List;

public class GuiManager extends Module implements Listener {

	private final List<Gui> menus = Lists.newArrayList();
	
	public GuiManager(JavaPlugin plugin) {
		super(plugin, "Gui Manager");

		registerListener(
				this
		);
	}
	
	@EventHandler
	public void on(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		int slot = event.getRawSlot();
		
		for(Gui menu : Lists.newArrayList(menus)) {
			if(event.getView().getTitle().equals(menu.getName())) {
				if(menu.getCurrentSessions().containsKey(player.getUniqueId())) {
					event.setCancelled(true);
					
					if(event.getCurrentItem() != null) {
						if(menu.getElements().containsKey(slot)) {
							menu.getElements().get(slot).click(player, event.getClick(), event.getInventory());
						}
					}
				}
			}
		}
	}

	public List<Gui> getMenus() {
		return menus;
	}

}
