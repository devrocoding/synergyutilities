package usa.synergy.utilities.assets.gui;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import usa.synergy.utilities.SynergyUtilitiesAPI;
import usa.synergy.utilities.assets.gui.object.GuiSize;
import usa.synergy.utilities.assets.utilities.ItemBuilder;

import java.util.*;
import java.util.Map.Entry;

public abstract class Gui {

	@Getter
	private final JavaPlugin plugin;
	private final String name;
	private final GuiSize guiSize;
	private final Map<Integer, GuiElement> elements;
	private Gui backGui = null;
	private final Map<UUID, Inventory> currentSessions;
	
	public Gui(JavaPlugin plugin, String name, GuiSize guiSize) {
		this.plugin = plugin;
		this.name = name;
		this.guiSize = guiSize;
		this.elements = Maps.newHashMap();
		this.currentSessions = Maps.newHashMap();

		SynergyUtilitiesAPI.getApi().getGuiManager().getMenus().add(this);
	}

	public abstract void setup();

	public Gui setBackGui(Gui backGui){
		this.backGui = backGui;
		return this;
	}

	public boolean hasBackGui(){
		return this.backGui != null;
	}

	public Gui getOuterClazz(){
		return this;
	}

	public GuiElement getBackGuiElement(){
		return hasBackGui() ? new GuiElement() {
			@Override
			public ItemStack getIcon(Player player) {
				return new ItemBuilder(Material.ARROW)
						.setName("§c§← Go Back")
						.build();
			}

			@Override
			public void click(Player player, ClickType clickType, Inventory inventory) {
				backGui.open(player, false);
			}
		} : null;
	}

	public Inventory open(Player player){
		return open(player, true);
	}
	
	private Inventory open(Player player, boolean setup) {
		if (setup)
			setup();
		Inventory inventory = Bukkit.createInventory(null, guiSize.getSlots(), name);

		for(Entry<Integer, GuiElement> element : elements.entrySet()) {
			inventory.setItem(element.getKey(), element.getValue().getIcon(player));
		}

		player.openInventory(inventory);

		for(Gui menu : SynergyUtilitiesAPI.getApi().getGuiManager().getMenus()) {
			if(!menu.equals(this)) {
				menu.getCurrentSessions().remove(player.getUniqueId());
			}
		}

		currentSessions.put(player.getUniqueId(), inventory);
		player.updateInventory();

		return inventory;
	}

	public void close(Player player) {
		player.closeInventory();
		currentSessions.remove(player.getUniqueId());
	}
	
	public void addElement(GuiElement element) {
		for (int i = 0; i < this.guiSize.getSlots(); ++i) {
			if (!this.elements.containsKey(i)) {
				this.addElement(i, element);
				return;
			}
		}
	}

	public void line(int first, int last, GuiElement element){
		for (int i=first;i<=last;i++){
			addElement(i, element);
		}
	}
	
	public boolean isFull() {
		for (int i = 0; i < this.guiSize.getSlots(); ++i) {
			if (this.getElement(i) == null) {
				return false;
			}
		}

		return true;
	}

	public int getCenter(){
		switch (this.guiSize){
			case TWO_ROWS:
				return 4;
			case THREE_ROWS:
				return 13;
			case FOUR_ROWS:
				return 13;
			case FIVE_ROWS:
				return 22;
			case SIX_ROWS:
				return 22;
		}
		return 4;
	}

	public void surroundWith(GuiElement item) {
		if (getSize() >= GuiSize.THREE_ROWS.getSlots()) {
			Integer[] walls = new Integer[]{9,17,18,26,27,35,36,44};
			List<Integer> slots = new ArrayList<>();
			final int size = this.guiSize.getSlots();

			// Outer walls
			int csize = size;
			for(int i=0;i<9;i++){
				slots.add(--csize);
				slots.add(i);
			}

			slots.addAll(Arrays.asList(walls));
			Object[] slotsArray = slots.toArray();
			Arrays.sort(slotsArray);

			for (Object obj : slotsArray) {
				int i = Integer.valueOf(obj.toString());

				if (i >= size){
					break;
				}
				if (getElement(i) == null) {
					addElement(i, item);
				}
			}
		}
	}

	public List<Integer> getCenterInput(){
		List<Integer> places = new ArrayList<>();

		switch (this.guiSize){
			case ONE_ROW:
				places.addAll(Arrays.asList(new Integer[]{1,2,3,4,5,6,7}));
				break;
			case TWO_ROWS:
				places.addAll(Arrays.asList(new Integer[]{1,2,3,4,5,6,7,10,11,12,13,14,15,16}));
				break;
			case THREE_ROWS:
				places.addAll(Arrays.asList(new Integer[]{10,11,12,13,14,15,16}));
				break;
			case FOUR_ROWS:
				places.addAll(Arrays.asList(new Integer[]{10,11,12,13,14,15,16,19,20,21,22,23,24,25}));
				break;
			case FIVE_ROWS:
				places.addAll(Arrays.asList(new Integer[]{10,11,12,13,14,15,16,19,20,21,22,23,24,25,28,29,30,31,32,32,33,34}));
				break;
			case SIX_ROWS:
				places.addAll(Arrays.asList(new Integer[]{10,11,12,13,14,15,16,19,20,21,22,23,24,25,28,29,30,31,32,32,33,34,37,38,39,40,41,42,43}));
				break;
		}
		return places;
	}

	public void addElement(int slot, GuiElement menuElement) {
		if (slot >= 0) {
			this.elements.put(Integer.valueOf(slot), menuElement);
		}
	}
	
	public void removeElement(int slot) {
		if(this.elements.containsKey(slot)) {
			this.elements.remove(slot);
		}
	}
	
	public GuiElement getElement(int slot) {
		if(elements.containsKey(slot)) {
			return elements.get(slot);
		}
		
		return null;
	}
	
	public String getName() {
		return name;
	}

	public int getSize() {
		return guiSize.getSlots();
	}

	public Map<Integer, GuiElement> getElements() {
		return elements;
	}

	public Map<UUID, Inventory> getCurrentSessions() {
		return currentSessions;
	}
	
}
