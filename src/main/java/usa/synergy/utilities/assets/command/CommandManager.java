package usa.synergy.utilities.assets.command;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.java.JavaPlugin;
import usa.synergy.utilities.Module;
import usa.synergy.utilities.assets.utilities.UtilString;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.bukkit.Bukkit.getServer;

public class CommandManager extends Module implements Listener {

    private final List<SynergyCommand> commands = Lists.newArrayList();
    private Map<String, Command> knownCommands;

    public CommandManager(JavaPlugin plugin) {
        super(plugin, "Command Manager");

//        if (!SynergyUtilitiesAPI
//                .getApi()
//                .getVersionManager()
//                .checkVersion(1.8)) {
//            registerListener(
//                new TabCompleteListener()
//            );
//        }

        try {
//            Object commandMap = getPlugin().getServer().getClass().getMethod("getCommandMap").invoke(getPlugin().getServer());
//            Field field = commandMap.getClass().getDeclaredField("knownCommands");
//            field.setAccessible(true);
//            this.knownCommands = (Map<String, SynergyCommand>) field.get(commandMap); // << 1.13

            final SimpleCommandMap commandMap = (SimpleCommandMap) getPrivateField(getServer().getPluginManager(), "commandMap");
            this.knownCommands = (HashMap<String, Command>) getPrivateField(commandMap, "knownCommands"); // Line 293
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Object getPrivateField(Object object, String field) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Field objectField = field.equals("commandMap") ?
                clazz.getDeclaredField(field) :
                field.equals("knownCommands") ?
                        Bukkit.getVersion().contains("1.14") ?
                                clazz.getSuperclass().getDeclaredField(field) :
                                clazz.getDeclaredField(field) :
                        null;
        objectField.setAccessible(true);
        Object result = objectField.get(object);
        objectField.setAccessible(false);
        return result;
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(PlayerCommandPreprocessEvent e) {
        List<String> args = UtilString.convert(e.getMessage().split(" "));
        String cmd = args.get(0).replaceAll("/", "");

        for(SynergyCommand command : commands) {
            if(Arrays.asList(command.getAliases()).contains(cmd.toLowerCase())) {
                e.setCancelled(true);
                args.remove(0);
                if (e.getPlayer().hasPermission(command.getPermission())) {
                    command.execute(e.getPlayer(), cmd, args.toArray(new String[args.size()]));
                    return;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(ServerCommandEvent e) {
        if (e.getSender() instanceof ConsoleCommandSender){
            List<String> args = UtilString.convert(e.getCommand().split(" "));
            String cmd = args.get(0).replaceAll("/", "");
            for(SynergyCommand command : commands) {
                if (command.isConsoleAllowed()) {
                    if (Arrays.asList(command.getAliases()).contains(cmd.toLowerCase())) {
                        e.setCancelled(true);
                        args.remove(0);
                        command.execute(((ConsoleCommandSender) e.getSender()), cmd, args.toArray(new String[args.size()]));
                        return;
                    }
                }
            }
        }
    }

    public boolean isCommand(String command){
        for(SynergyCommand cmd : commands){
            for(String alias : cmd.getAliases()){
                if (alias.equalsIgnoreCase(command)){
                    return true;
                }
            }
        }
        return false;
    }

    public void unregisterMinecraftCommand(String command) {
        Bukkit.getScheduler().runTaskLater(getPlugin(), () -> {
            this.knownCommands.remove(command);
            this.knownCommands.remove("minecraft:" + command);
            this.knownCommands.remove("bukkit:" + command);
            this.knownCommands.remove("essentials:" + command);
            this.knownCommands.remove("essentials:e" + command);
        }, 1L);
    }

    public List<SynergyCommand> getCommands() {
        return commands;
    }
}
