package usa.synergy.utilities.service.sql;

import lombok.Getter;
import usa.synergy.utilities.service.SQLService;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class DatabaseManager {

    @Getter
    private Connection connection;
    @Getter
    private SQLService sqlService;
    @Getter
    private String prefix = "synergy";

    public DatabaseManager(SQLService service, String prefix){
        this.sqlService = service;
        if (prefix != null && !prefix.equals("")) {
            this.prefix = prefix;
        }

        try{
            // Connect to SQL
            this.connect();

        }catch (SQLException e){
            e.printStackTrace();
//            Bukkit.getPluginManager().disablePlugin(SynergyUtilities.getSynergyUtilitiesAPI().getJavaPlugin());
        }
    }

    public boolean connect() throws SQLException{
        if (this.connection != null && !connection.isClosed()){
            return false;
        }
        if (getSqlService().isIniatialized()) {
//            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection(getSqlService().getDriver()
                    + "/" + getSqlService().getDatabase(), getSqlService().getUsername(), getSqlService().getPassword());
            return true;
        }else{
            throw new SQLException("SQL Information is wrong or empty! Check 'Settings.yml'");
        }
    }

    public void disconnect() throws SQLException {
        if (!this.connection.isClosed()) {
            this.connection.close();
        }
    }

    public boolean update(String table, Map<String, Object> data, String where){
        HashMap<Integer, Object> indexed = new HashMap<>();
        try {
            connect();
            StringBuilder query = new StringBuilder("UPDATE "+getPrefix()+"_"+table+" SET ");
            StringBuilder whereQuery = new StringBuilder(" WHERE "+where);

            int a=1;
            for(String key : data.keySet()){
                if (a>1) query.append(", ");
                Object value = data.get(key);
                query.append(key+"=?");

                indexed.put(a, value);
                a++;
            }

            query.append(whereQuery.toString());

            PreparedStatement preparedStatement = getConnection().prepareStatement(query.toString());

            for(Integer index : indexed.keySet()){
                Object value = indexed.get(index);

                preparedStatement.setObject(index, value);
            }

            preparedStatement.executeUpdate();
            disconnect();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    public ResultSet getResults(String table, String where, Map<Integer, Object> data) throws SQLException {
        ResultSet resultSet = getResults(getPrefix(), table, where, data);
        return resultSet;
    }

    public ResultSet getResults(String tablePrefix, String table, String where, Map<Integer, Object> data) throws SQLException {
        connect();

        StringBuilder query = new StringBuilder(
                "SELECT * FROM "+(tablePrefix==null?"":tablePrefix+"_")+table+(where != null ? (" WHERE "+where) : "")
        );

        PreparedStatement statement = getConnection().prepareStatement(
            query.toString()
        );

        if (where != null) {
            for (int b : data.keySet()) {
                Object object = data.get(b);
                statement.setObject(b, object);
            }
        }

        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }

    public void executeQuery(String query) {
        try {
            connect();
        }catch (SQLException e){
            e.printStackTrace();
        }
        try {
            getConnection().prepareStatement(query).execute();
            disconnect();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public boolean insert(String table, Map<String, Object> data){
        return execute("INSERT INTO", table, data, true);
    }

    public boolean execute(String prefix, String table, Map<String, Object> data, boolean insert) {
        HashMap<Integer, Object> indexed = new HashMap<>();
        try {
            connect();
            StringBuilder query = new StringBuilder(prefix+" "+getPrefix()+"_"+table+" ("),
            values = new StringBuilder(") VALUES(");

            int a=1;
            for(String key : data.keySet()){
                if (a>1) {
                    query.append(", ");
                    values.append(", ");
                }
                query.append("`"+key+"`");
                values.append('?');

                indexed.put(a, data.get(key));
                a++;
            }

            values.append(")");
            query.append(values.toString());

            PreparedStatement preparedStatement = getConnection().prepareStatement(query.toString());

            for(Integer index : indexed.keySet()){
                Object value = indexed.get(index);

                preparedStatement.setObject(index, value);
            }

            preparedStatement.executeUpdate();
            disconnect();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

}
