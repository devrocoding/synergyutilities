package usa.synergy.utilities.service;

import lombok.Getter;

public class SQLService {

    @Getter
    private String driver, username, password, database;
    @Getter
    private boolean iniatialized;

    public SQLService(String driver, String database, String username, String password){
        this.driver = driver;
        this.database = database;
        this.username = username;
        this.password = password;

        if (driver != null && database != null && username != null)
            this.iniatialized = true;
    }

}
